# Administracion centralizada sala informatica perteneciente al Estado.

**TITULO:**
*Administracion centralizada sala informatica perteneciente al Estado.*

**AUTOR:**
*Milton Huguenet*

**TRAYECTO:**
*Administración*

**DIRECTOR:**
*Emiliano Lopez, Maximiliano Boscovich*

**RESUMEN:**
*La propuesta surge depués de pensar en como automatizar y administrar centralizadamente todas las pcs que allá en la sala desde un servidor, ya que administrar manualmente grandes nodos es un trabajo arduo y tedioso, esto mejoraría y agilizaría tiempo, trabajo, aprovisionamiento de pcs, entre otros. Pensé en esto y creí que es muy importante por que no se dispone de un administrador fijo esta cambiando todo el tiempo imagínense todo el trabajo y tiempo que le toma a cada administrador, reconocer el hardware de todas las pcs, luego ponerse al día con las actualizaciones formateos o generar un script para la creación de un firewall*
*con las mismas políticas o no, en todas las maquinas bueno para intentar ahorrarnos todo lo mencionado y demas cosas que vallan surgiendo con el correr del tiempo puesto que la tecnología se actualiza constantemente, intentare hacer esta administración con herramientas de Software Libre e intentare dejar todo documentado en un "manual" para que todos los que los quieran implementar esta tarea lo puedan hacer lo mas sencillo posible, mi intención es compartirlo por GitLab usando la licencia Atribución-Compartir Igual (CC BY-SA) Reconocimiento – Compartir Igual (bysa): Se permite el uso comercial de la obra y de las posibles obras derivadas, la distribución de las cuales*
*se debe hacer con una licencia igual a la que regula la obra original.*

**JUSTIFICACIÓN:**
*El problema surge en una sala con 10 pcs mas o menos siempre con posibilidad de ir aumentando, todas están en constante funcionamiento durante casi todo el día con Gnu/Linux, pensando en como mantenerlas actualizadas, en como dejarlas con las configuraciones deseadas(usuarios, acceso ssh, comandos sudo, programas mas utilizados, apagados automáticos, firewall, etc), cada ves que se agregue una pc nueva, cada ves que sea formateada, o cuando se cambie de administrador*
*, entre otras cosas, acá entra en juego la idea pensada que consiste en la administración de todas las pcs desde una central o servidor mediante una herramienta de Software Libre llamada ANSIBLE, esto consiste en administrar las configuraciones de los nodos utilizando un método definido, manteniendo así el sistema y su integridad a lo largo del tiempo, la idea solucionaría la mayoría de los problemas pensados, porque esta herramienta nos permitiría crear una receta(playbook o libro de jugadas)*
*en donde definiríamos las configuraciones de  como queremos que las pcs queden después de aplicarle dicha receta, esto ahorraría tiempo ya que no se tendría que configurar manualmente una por una, agilizaría las tareas del administrador de "turno", las pcs estarían siempre disponibles y configuradas para su uso, creo que también una vez que la receta este creada y lista, cualquier usuario final con autorización podrá aplicar esa receta sin la necesidad de el administrador, esto aportaría mucho a la sala*
*ya que las pcs están en constante uso y no siempre hay un administrador disponible por cualquier evento que surja.*
*Existen muchas herramientas para realizar esta tarea (por ejemplo Puppet, CFEngine, Chef, entre otras), yo elegí ANSIBLE, por varios motivos el principal porque es declarativa esto quiere decir que no damos instrucciones sobre los pasos que debe seguir para configurar los nodos, sino que le indicamos el estado de como queremos encontrar el nodo una ves aplicada la receta, otro motivo, es relativamente menos complicada que las demás y otro pero no menos importante es que no necesita instalar ningún tipo de*
*software agente en el "cliente" solo tener, ssh y python instalados.*

**ESTADO DEL ARTE:**
*Después de una intensa búsqueda de bibliográfica(antecedentes, pruebas, experiencias, etc.), sobre la Administración Centralizada y el uso de la herramienta Ansible pude encontrar las siguientes experiencias:*
*Articulo de Barrios, F.(2019). Automatización de arquitecturas TI con ANSIBLE. Automatización de arquitecturas TI con ANSIBLE. Donde habla un poco de lo que es la herramienta, ventajas, contras y algunos conceptos. Link:https://backtrackacademy.com/articulo/ansible-automatizacion-de-ti*

*También pude encontrar este trabajo de Vizcaíno, A, M.(2016). DJBot a la batuta: Salas de computadoras hechas orquesta. Este trabajo nos acerca mas a la automatización de tareas ya que consta de la administración centralizada mediante Ansible y DJBot una aplicación Web para hacer mas sencilla y dinámica aun la automatización y AD(Administración Centralizada), deja un poco masen claro la importancia de la automatización en ves de la realización manual de la Administración, conocer un el amplio abanico de*
*herramientas donde también se estudió la realidad cambiante que se vive en el sector de las tecnologías de la información, y se descubrieron y analizaron varias herramientas disponibles para reducir o, al menos, simplificar el trabajo de administradores de sistemas.*

*Por ultimo esta investigación y producción de López, Aguilar, F.J.(2015). Aprovisionamiento automático de infraestructura y de configuración de la infraestructura. El caso Adblock Plus. Aquí observamos que empresas muy importantes como Spotify, Sony, Rackspace, IndieGoGo, CheezBurguer, Facebook, Apple o la NASA utilizan algunas de las herramientas de Administración Centralizada como, Puppet, chef, Ansible, etc. y ninguna deja evidenciado en escrito(documentación clara) como realizan sus tareas mediante el uso*
*de estas herramientas lo que me motiva mas a realizar mi trabajo para así poder dejarlo evidenciado lo mas claro posible para su reutilización. También se puede constatar la importancia de creación y trabajo sobre Infraestructuras Desechables estas crean material y herramientas de capacitación para los usuarios que no están familiarizados con estas herramientas.*

**LICENCIA DE LA DOCUMENTACIÓN:**
*está licenciando bajo LICENCIA CC BY-SA 4.0 Internacional*
*Usted es libre de:*
*• Compartir - copiar y redistribuir el material en cualquier medio o formato.*
*• Adaptar - Combinar, modificar y generar sobre el material para cualquier propósito, incluso con fines comerciales.*
*El licenciante no puede revocar estas libertades, siempre y cuando siga los términos de la licencia.*
*Bajo los términos siguientes :*
*• Atribución - Usted debe dar el prestigio apropiado, proporcionar un enlace a la licencia, e indicar si se han realizado cambios. Usted puede hacerlo en cualquier forma razonable, pero no de cualquier*
*manera que sugiera que el licenciante le da sustento a usted o su uso.*
*• Compartir bajo la misma - Si combina, modifica o amplia el material, se debe distribuir sus contribuciones bajo la misma licencia que la original.*
*No hay restricciones adicionales – Usted no puede aplicar condiciones legales o medidas tecnológicas que legalmente prohíban los permisos de licencia.*

***OBJETIVOS:***
*Tal como se ha explicado en la contextualización, la etapa de administrar nodos y aprovisionar software es larga, tediosa y con demasiadas casuísticas que pueden provocar un error. Con este proyecto se quiere agilizar la facilidad y temporalidad de las tareas y/o servicios. Por tanto, el objetivo de este proyecto consiste en:*

*Objetivo general:*

*  *Investigación: Administración tradicional, Administración automática, herramientas y software actuales para llevar adelante esta tarea.*

*  *Se busca implementar y mejorar la administración así como el aprovisionamiento de  software automático/centralizado en la sala de informática con sistemas operativos GNU/Linux por medio de una herramienta de Software Libre llamada Ansible y con una necesidad mínima de que el administrador/usuario intervenga en dichas tareas.*

*  *Realizar un “manual” en donde quede lo mas detallado y sencillo posible todo lo hecho, para su reutilización.*

*Objetivos Específicos:*

*  *Planeación del trabajo.*

*  *Estudiar las diferentes tipos de administración y herramientas, estudiar la herramienta Ansible.*

*  *Integración: estudiar el hardware disponible en la sala(pcs) y preparar un entorno “virtual” acorde al físico para probar primeramente la administración centralizada con Ansible y verificar su adecuado funcionamiento.*

*  *Implementar la administración física en la sala y verificar su adecuado funcionamiento.*

*  *Resultados, encuesta y conclusión.*

*  *Documentación.*

***Metodología y Cronograma:***
*La duración estimada del proyecto es de aproximadamente 6 meses, el mismo comienza en junio y inaliza en diciembre. Es importante considerar que esta planificación inicial se actualizará al final del proyecto ya que pueden aparecer obstáculos y desvíos temporales.*

1.  *Nombre:    Planeación del trabajo     -       Duración:   1 mes.*

2.  *Nombre:    Estudiar las diferentes tipos de administración y herramientas, estudiar la herramienta Ansible -   Duración:   1 mes.*

3.  *Nombre:    Integración: estudiar el hardware disponible en la sala(pcs) y preparar un entorno “virtual” acorde al físico para probar primeramente la administración centralizada con Ansible y verificar su adecuado funcionamiento    -   Duración:   1 mes.*

4.  *Nombre:    Implementar la administración física en la sala y verificar su adecuado funcionamiento  -   Duración:   1 mes.*

5.  *Nombre:    Resultados, encuesta y conclusión   -   Duración:   1 mes.*

6.  *Nombre:    Documentación   -   Duración:   1 mes.*